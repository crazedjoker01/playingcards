#include <conio.h>
#include <iostream>

using namespace std;

enum Suit
{
	SPADES, HEART, DIAMONDS, CLUBS
};

enum Rank
{
	TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

struct Card
{
	Suit suit;
	Rank rank; 
};